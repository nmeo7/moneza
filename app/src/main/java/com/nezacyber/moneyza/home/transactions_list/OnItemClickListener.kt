package com.nezacyber.moneyza.home.transactions_list

import com.nezacyber.moneyza.util.Message

interface OnItemClickListener {
    fun onItemClick(item: Message?)
}