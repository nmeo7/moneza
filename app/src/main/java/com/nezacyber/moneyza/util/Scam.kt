package com.nezacyber.moneyza.util

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.math.BigInteger
import java.security.MessageDigest

open class Scam : RealmObject() {
    @PrimaryKey
    var hash: String? = null
    var text: String? = null
    var phoneNumber: String? = null
    var seen: Boolean? = null

    fun makeHash () {
        val md = MessageDigest.getInstance("MD5")
        val txt = "$phoneNumber>>>$text"
        hash = BigInteger(
                1,
                md.digest(txt.toByteArray()))
                .toString(16).padStart(32,
                        '0')
    }
}