package com.nezacyber.moneyza.util

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.TaskStackBuilder
import com.nezacyber.moneyza.*
import java.util.*


class MessageReceiver : BroadcastReceiver() {

    companion object MessageReceiver

    private val TAG: String = "Sms Receiver"
    val pdu_type = "pdus"

    fun setMainActivity(mainActivity: MainActivity)
    {
        this.activity = mainActivity
    }

    var activity: MainActivity? = null

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    override fun onReceive(context: Context, intent: Intent) {

        // val persistence = DataPersistence(this)

        // Get the SMS message.
        val bundle = intent.extras
        val msgs: Array<SmsMessage?>
        var strMessage = ""
        val format = bundle!!.getString("format")


        // Log.d(TAG, "onReceive: message message!!")

        // Retrieve the SMS message received.
        val pdus = bundle[pdu_type] as Array<Any>?
        if (pdus != null) {
            // Check the Android version.
            val isVersionM = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            // Fill the msgs array.
            msgs = arrayOfNulls<SmsMessage>(pdus.size)

            var msgBody = ""
            var sender = ""
            for (i in msgs.indices) {
                // Check Android version and use appropriate createFromPdu.
                if (isVersionM) {
                    // If Android version M or newer:
                    msgs[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray, format)
                } else {
                    // If Android version L or older:
                    msgs[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                }
                // Build the message to show.
                if (msgs[i]?.originatingAddress != null)
                    sender = msgs[i]?.originatingAddress!!
                msgBody += msgs[i]?.messageBody.toString()
                // Log and display the SMS message.
            }

            // Log.i("OnMessage", "$sender: $msgBody")
            var message = ""

            if (sender == "M-Money")
            {
                val m = ParseMessage().parseMessage(msgBody)
                // maybe here find a way to save data locally before persisting it to the realm database
                var saved = false

                if (m.type == "PAYMENT")
                {
                    val sharedPref = context.getSharedPreferences(
                        "number_amount",
                        Context.MODE_PRIVATE
                    )
                    val amount = sharedPref.getString("AMOUNT", "0")
                    val number = sharedPref.getString("NUMBER", "")
                    val time = sharedPref.getLong("TIME", Long.MAX_VALUE)
                    message = sharedPref.getString("MESSAGE", "")!!

                    if (m.amount == amount?.toInt() && (System.currentTimeMillis() < time + 1000 * 120))
                    {
                        m.subjectNumber = number
                        m.message = message
                        DataPersistence(context).save(m)
                        // Log.i("Connect", m.toString())
                        saved = true
                    }

                    // reset the shared prefs
                    with(sharedPref?.edit()) {
                        this?.putString("NUMBER", "")
                        this?.putString("AMOUNT", "")
                        this?.putString("MESSAGE", "")
                        this?.putLong("TIME", 0)
                        this?.apply()
                    }
                }

                // if (activity != null)
                    // activity?.notifySmsReceived(m)

                if (!saved)
                    DataPersistence(context).save(m)

                if (message != "")
                    showNotification(context, "How did you like what you paid for?")
            }
            else {
                val m = ParseMessage().parseMessage(msgBody)
                if (m.type != null && m.type != "")
                    scamDetected(context, sender, msgBody)
                else {
                    val sanitizedMessage = SanitizedMessage()

                    val re = Regex("[^a-z ]")
                    msgBody = msgBody.toLowerCase(Locale.ROOT)
                    msgBody = re.replace(msgBody, "") + " " + re.replace(msgBody, " ")
                    val words = msgBody.split(" ")

                    for (word in words)
                        if (sanitizedMessage.checkWords(word))
                        {
                            scamDetected(context, sender, msgBody)
                            return
                        }
                }
            }
        }
    }

    private fun scamDetected(context: Context, phoneNumber: String, text: String)
    {
        val dataPersistence = DataPersistence(context)

        val scam = Scam()
        scam.phoneNumber = phoneNumber
        scam.text = text
        scam.seen = false
        scam.makeHash()

        dataPersistence.saveScam(scam)
        showNotification(context, "Scam detected!!!")
    }

    private fun createNotificationChannel(context: Context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = "all_notifications" // You should create a String resource for this instead of storing in a variable
            val mChannel = NotificationChannel(
                channelId,
                "General Notifications",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            mChannel.description = "This is default channel used for all other notifications"

            val notificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }
    }

    private fun showNotification(context: Context, text: String) {
        createNotificationChannel(context)

        val actionIntent = Intent(context, MessageReceiver::class.java).apply {
            action = "OK"
        }


        val channelId = "all_notifications" // Use same Channel ID
        val intent = Intent(context, MainActivity::class.java)
        // val pendingIntent = PendingIntent.getActivity(context, 123, actionIntent, 0);

        // Create an Intent for the activity you want to start
        // val resultIntent = Intent(context, ResultActivity::class.java)
        val resultIntent = Intent(context, NotificationService::class.java)
// Create the TaskStackBuilder
        val resultPendingIntent: PendingIntent? = TaskStackBuilder.create(context).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(intent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val acceptPendingIntent = PendingIntent.getService(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)


        val builder = NotificationCompat.Builder(context, channelId) // Create notification with channel Id
                .setSmallIcon(R.drawable.ic_wallet_icon)
                .setContentTitle(text)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .addAction(R.drawable.ic_back, "Good", acceptPendingIntent)
                // .setAutoCancel(true)
                .addAction(R.drawable.ic_back, "Fair", acceptPendingIntent)
                // .setAutoCancel(true)
                .addAction(R.drawable.ic_delete, "Bad", acceptPendingIntent)
                // .setAutoCancel(true)
        builder.setContentIntent(acceptPendingIntent)
        // builder.setAutoCancel(true)
        // val mNotificationManager = Message.context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // with(mNotificationManager) {
            // notify(123, builder.build())
        // }

        with(NotificationManagerCompat.from(context)) {
            notify(123, builder.build())
        }

    }
}