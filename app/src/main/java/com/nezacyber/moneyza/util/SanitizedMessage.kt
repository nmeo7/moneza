package com.nezacyber.moneyza.util

class SanitizedMessage {

    val scamWords = listOf("mobile money account", "received", "balance", "wakiriye", "watsindiye")

    private fun LevenshteinDistance (w1: String, w2: String) : Int
    {
        val d = Array(w1.length) { Array(w2.length) { 0 } }

        for (i in 0..w1.length)
            d[i][0] = i

        for (j in 0..w2.length)
            d[0][j] = j

        var substitutionCost = 0

        for (i in 0..w1.length)
            for (j in 0..w2.length)
                if (w1[i] != w2[j])
                {
                    substitutionCost = 1
                    var least = d[i-1][j] + 1
                    if (d[i][j-1] + 1 < least)
                        least = d[i][j-1]
                    if (d[i-1][j-1] + substitutionCost < least)
                        least = d[i-1][j-1] + substitutionCost

                    d[i][j] = least
                }

        return d[w1.length - 1][w2.length - 1]
    }

    fun checkWords (word: String) : Boolean
    {
        var check1 = 10

        val scamWords = listOf("account", "received", "balance", "wakiriye", "watsindiye", "utsindiye", "woherejwe", "wishyuwe")
        for (x in scamWords)
        {
            val r = LevenshteinDistance(word, x)
            if (r < check1)
                check1 = r
        }

        if (check1 < 3)
            return true

        return ((LevenshteinDistance(word, "amafaranga") < 3) || (LevenshteinDistance(word, "money") < 3)) &&
                (check1 > 2)
    }
}