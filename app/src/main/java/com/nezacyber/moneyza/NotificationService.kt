package com.nezacyber.moneyza

import android.app.IntentService
import android.app.NotificationManager
import android.content.Intent
import android.content.Intent.getIntent
import android.util.Log


const val ACTION_FOO = "com.rmsoft.moneza.action.FOO"
const val ACTION_BAZ = "com.rmsoft.moneza.action.BAZ"

class NotificationService : IntentService("NotificationService") {

    override fun onHandleIntent(intent: Intent?) {
        // Log.i("NOTIFICATION", "HERE WE ARE")


        val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(123)
    }
}