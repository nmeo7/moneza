# MoNeza

## privacy policy

The app can read all your messages and can place phone calls, but this isn't done maliciously in any ways, and not a single bit of your information is leaked; you are totally safe.

### Purposes for reading messages:

MoNeyza needs to read your mobile money messages in order to be able to organize your spendings and earnings history, as well as make a dashboard that will help you visualize it easily.

### Purposes for making phone calls:

MoNeyza needs to dial USSD codes, which are considered as phone calls, to run Mobile Money short codes for payments and other enquiries, and also for dialing listed emergency and useful numbers.


## For iOS users:

sorry, your phone wasn't made for the African market, unless the involved parties collaborate. 


The work is still in progress; we are not there yet